<?php

class EmporioEnvios {

    /**
     * EmporioEnvios Base url
     */
    const BASE_URL = 'https://ar.emporioenvios.com/api';
    /**
     * Api Key of your EmporioEnvios Account
     */
    const API_KEY = ''; 
    /**
     * Api Secret of your EmporioEnvios Account
     */
    const API_SECRET = '';
    /**
     * EmporioEnvios Account's ID
     */
    const ACCOUNT_ID = '';

    /**
     * Get Shipment's information
     *
     * @param $id Integer Shipment's id
     *
     * @return Object
     */
    public function getShipment($id)
    {
        $resource = '/shipment/'.$id.'?account_id='.static::ACCOUNT_ID;

        return static::executeRequest($resource, 'GET', null);
    }

    /**
     * Create a Shipment in EmporioEnvios
     *
     * @param $data Object shipment creation datatype
     *
     * @return Object
     */
    public function createShipment($data)
    {
        $resource = '/shipment/create?account_id='.static::ACCOUNT_ID;
        return static::executeRequest($resource, 'POST', $data);
    }

    /**
     * Get Quote of a Shipment througt EmporioEnvios
     *
     * @param $data Object shipment quote datatype
     *
     * @return Object
     */
    public function quoteShipment($data)
    {
        $api_path = '/shipment/quote?account_id='.static::ACCOUNT_ID;
        return static::executeRequest($api_path, 'POST', $data);
    }

    /**
     * Update a Shipment status
     *
     * @param $data Object shipment object datatype
     *
     * @return Object
     */
    public function updateStatus($data)
    {
        $api_path = '/shipment/status?account_id='.static::ACCOUNT_ID;
        return static::executeRequest($api_path, 'POST', $data);

    }

    /**
     * Get Shipment's labels and remitos
     * 
     * @param $data Object shipment creation datatype
     *
     * @return Object
     */
    public function getShipmentLabels($external_id, $format)
    {
        $resource = '/shipment/label?account_id='.static::ACCOUNT_ID.'&external_id='.$external_id.'&format='.$format;

        return static::executeRequest($resource, 'GET', null);
    }

    /**
     * Execute the request
     *
     * @param $resource String api resource's route
     * @param $method String request's http method
     * @param $post_data Object data for post and put request (Optional)
     *
     * @return Object
     */
    private static function executeRequest($resource, $method, $post_data = null)
    {
        $date = new DateTime();

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, static::BASE_URL.$resource);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Auth-Date: '.$date->format("Y-m-d H:i:s"),
            'Auth-Key: '.static::API_KEY,
            'Auth-Signature: '.static::generateToken($date->format("Y-m-d H:i:s")),
            'Content-Type: application/json',
        ]);

        if(!is_null($post_data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        }

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);
        $error = curl_error($ch);

        curl_close($ch);

        return json_decode($response);
    }

    /**
     * Generate api's token signature for request headers
     *
     * @param $datetime DateTime in UTC format
     *
     * @return String api signature
     */
    public static function generateToken($datetime)
    {
        return hash_hmac('sha256', $datetime, static::API_SECRET, false);
    }

}