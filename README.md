# SDK de PHP para integración con Emporio Envios


Instalación 
---

### Composer

Agrega la siguiente línea dentro de la sección require de tu ***composer.json***   
`"emporioenvios/emporio-envios": "dev-master"`
	
Corre el comando `composer update` y una vez finalizado el proceso podrás ver el directorio "emporioenvios" dentro de sus vendors.

### Descargar las fuentes
También puedes descargar los archivos de la carpeta src del reporitorio.


## Configuración

Desde tu cuenta, obtén tu *api key* y *api secret*.
Necesitarás que tu contacto comercial te facilite algunos datos adicionales como tu `account_id`, y los ids de tus origenes de envío.



## Documentación Adicional

Puedes ver la [documentación de la api](https://ar.emporioenvios.com/documentation) en nuestro sitio.